library(rgdal)
library(raster)
library(rasterVis)
library(maptools)
library(png)
library(grDevices)
library(grid)
library(RODBC)
library(sqldf)
library(sp)
library(RPostgreSQL)
library(tidyr)
library(lubridate)
library(ggplot2)
library(rgeos)
library(plyr)
library(dplyr)
library(data.table)
library(jpeg)
library(extrafont)




Generate_Shapefile <- function(LocationSetId, CommodityId) {
  
  cn<-odbcDriverConnect("Driver=ODBC Driver 11 for SQL Server; Server=data3dev; Database=CommodityStagingData; trusted_connection=yes")
  
  for(i in 1:length(LocationSetId)) {
    
    sqlToExecute <- paste0("SELECT [LocationId]
                             FROM [CommodityStagingData].[dbo].[LocationSetMembers]
                             WHERE LocationSetId = ", LocationSetId[i])
    
    #Get the records from the output table
    setLocations <- as.data.table(sqlQuery(cn,sqlToExecute,errors=FALSE))
    setkey(setLocations, LocationId)
    
    sqlToExecute <- paste0("SELECT LocationId 
                           FROM [CommodityStagingData].[dbo].[WCRCommodityAreas]
                           WHERE IsProduction80 = 1 AND CommodityId = ", CommodityId)
    
    
    commodityLocations <- as.data.table(sqlQuery(cn, sqlToExecute, errors=F))
    setkey(commodityLocations, LocationId)
    
    areas <- setLocations[commodityLocations, nomatch = 0]
    
    
    locationSetToQuery <- areas$LocationId[1]
    j = 2
    while(j <= nrow(areas)) {
      locationSetToQuery <- paste0(locationSetToQuery, ",", areas$LocationId[j])
      j <- j + 1
    }
    
    
    sqlToExecute <- paste0("SELECT [LocationId]
                            ,[ShapeWKT]
                            ,[CentroidWKT] 
                           FROM CommodityStagingData.dbo.GlobalGridData
                           WHERE LocationId in (", locationSetToQuery, ")")
    
    gridShapes <- sqlQuery(cn, sqlToExecute, errors=F)
    
    
    
    data <- gridShapes
    
    data$ShapeWKT <- as.character(data$ShapeWKT)
    data$ShapeWKT <- substr(data$ShapeWKT, 11, nchar(data$ShapeWKT)-2)
    data$ShapeWKT <- gsub(",", "", data$ShapeWKT)
    data$ShapeWKT <- strsplit(data$ShapeWKT, " ")
    data <- unique(data)
    squares <- matrix(ncol = 10, nrow = nrow(data))
    ID <- data$LocationId
    
    for(k in 1:nrow(data)) {
      
      squares[k,] <- as.numeric(data$ShapeWKT[[k]])
      
    }
    
    squares <- unique(squares)
    ID <- unique(ID)
    
    polys <- SpatialPolygons(mapply(function(poly, id) {
      xy <- matrix(poly, ncol=2, byrow=TRUE)
      Polygons(list(Polygon(xy)), ID=id)
    }, split(squares, row(squares)), ID))
    
    spData <- SpatialPolygonsDataFrame(polys, data.frame(id=ID, row.names=ID))
    
    spDataForShpOut <- merge(spData, data, by.x="id", by.y="LocationId")
    
    spDataForShpOut <- subset(spDataForShpOut, select = c("id"))
    
    
    sqlToExecute <- paste0("SELECT [LocationSetName]
                            FROM [CommodityStagingData].[dbo].[LocationSets]
                           WHERE LocationSetId = ", LocationSetId[i])
    name <- sqlQuery(cn, sqlToExecute, errors=F)
    name <- gsub("Subset", "", as.character(name$LocationSetName[1]))
    
    
    
    sqlToExecute <- paste0("SELECT [CommodityName]
                           FROM [CommodityStagingData].[dbo].[WCRCommodities]
                           WHERE CommodityId = ", CommodityId)
    commodity <- sqlQuery(cn, sqlToExecute, errors=F)
    commodity <- as.character(commodity$CommodityName[1])
    
    
    
    #Set name for shapefile
    shapefileName = paste(name, commodity, sep="_")
    #Delete any previous shapefiles that have same name
    unlink(paste0(shapefileName, ".*"))
    #Write the shapefile
    writeOGR(spDataForShpOut ,".",shapefileName,driver="ESRI Shapefile")
  }
  odbcCloseAll()
}

