With AccumByYear as (
Select D.LocationId, round(D.TemporalId,-6) as Year,
  SUM(Precip) as AccPrec, Count("Precip") as CountRecords,
  SUM(libs.calc_pet(0, D.LocationId, DATE(LEFT(CAST(D.TemporalId as VARCHAR(10)),8)),
  D.TemporalId, D.Latitude, D.Longitude, 0,
  D.MaxMorningWind, D.MaxRelativeHumidity, D.MaxTemp, D.MaxWind, (case when D.MeanWindSpeed is null then D.MaxWind/2 else D.MeanWindSpeed end),
  D.MinRelativeHumidity, D.MinTemp, D.Precip, D.Solar, D.Elevation)) as AccPET
From weather.vAsia_daily_grid D
  INNER JOIN
  libs.LocationSetMembers LSM
  ON D.LocationId = LSM.LocationId
Where D.TemporalId%1000000 between 082700 and 092500
  and LSM.LocationSetId = 1
  and TemporalId >= 2006010100
Group by D.LocationId, round(D.TemporalId,-6)
), MinMaxYears as (
Select  Min(Year) as MinYear, Max(Year) as MaxYear
From
  (
  Select D.LocationId, round(D.TemporalId,-6) as Year
  from weather.asia_daily_grid D
    INNER JOIN
    libs.LocationSetMembers LSM
    ON D.LocationId = LSM.LocationId
  Where D.TemporalId%1000000 between 082700 and 092500
    and LSM.LocationSetId = 1
    and TemporalId >= 2006010100
  Group by D.LocationId, round(D.TemporalId,-6)
  ) X
)
Select LT.LocationId,
  CY.CYAccPrec, CY.CYAccPET, CY.CYAccPOverPET,
  LY.LYAccPrec, LY.LYAccPET, LY.LYAccPOverPET,
  LT.LTAccPrec, LT.SDAccPrec, LT.LTAccPET, LT.SDAccPET, LT.LTAccPOverPET, LT.SDAccPOverPET,
  (case when LT.SDAccPrec = 0 then 0 else (CY.CYAccPrec - LT.LTAccPrec)/LT.SDAccPrec end) as Z_SumPrec,
  (case when LT.SDAccPET = 0 then 0 else (CY.CYAccPET - LT.LTAccPET)/LT.SDAccPET end) as Z_SumPET,
  (case when LT.SDAccPOverPET = 0 then 0 else (CY.CYAccPOverPET - LT.LTAccPOverPET)/LT.SDAccPOverPET end) as Z_SumPOverPET
From
  (Select LocationId, AccPrec as LYAccPrec, AccPET as LYAccPET,
   (case when AccPET = 0 then NULL else AccPrec/AccPET end) as LYAccPOverPET
  From AccumByYear Y, MinMaxYears NX
  Where Y.Year = NX.MaxYear - 1000000) LY,
  (Select LocationId, AccPrec as CYAccPrec, AccPET as CYAccPET,
  (case when AccPET = 0 then NULL else AccPrec/AccPET end) as CYAccPOverPET
  From AccumByYear Y, MinMaxYears NX
  Where Y.Year = NX.MaxYear) CY,
  (Select
        LocationId, Avg(AccPrec) as LTAccPrec, stddev(AccPrec) as SDAccPrec,
        Avg(AccPET) as LTAccPET, stddev(AccPET) as SDAccPET,
        Avg(case when AccPET = 0 then NULL else AccPrec/AccPET end) as LTAccPOverPET,
        stddev(case when AccPET = 0 then NULL else AccPrec/AccPET end) as SDAccPOverPET
  From AccumByYear Y, MinMaxYears NX
  Where Y.Year between NX.MinYear and NX.MaxYear - 1000000
  Group by LocationId) LT
Where LY.LocationId = LT.LocationId
and LY.LocationId = CY.LocationId;
