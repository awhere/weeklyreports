library(rgdal)
library(raster)
library(rasterVis)
library(maptools)
library(png)
library(grDevices)
library(grid)
library(RODBC)
library(sqldf)
library(sp)
library(RPostgreSQL)
library(tidyr)
library(lubridate)
library(ggplot2)
library(rgeos)
library(plyr)
library(dplyr)
library(data.table)
library(jpeg)
library(extrafont)

source("External_Commodity_Functions.R")
source("GetDataForPAndPETAccumulationsWithinPeriods.R")

#font_import()
loadfonts(device="win")





###################################################
#
# Function that generates maps and charts
# given a time period and location set
#
# Parameters:
#   timePeriodIdToRun: Integer Time period to 
#     run for, relates to values stored in SQL
#   locationSetId: vector of integer location
#     ids that correspond to locations stored
#     in SQL table
#
# Example function call:
#   Generate_Charts("2015-12-31", 30, c(5, 6, 10))
#     Would create charts and maps for the current
#     time period for Uganda, Vietnam and S. Africa
#
#
#
#  To see possible location sets:
#   getLocationSets()
#
#  To see possible time periods:
#   getTimePeriods()
#
#
###################################################


Generate_Charts <- function(endDate = Sys.Date() - days(2), daysToRun = 30, locationSetId = NA, commodityIdToUse = NA, startYearTemporalId = 2006010100){
  
  print(locationSetId)
  print(is.na(commodityIdToUse))
  #########################################
  ##Read in files from local file system###
  #########################################
  print(getRangeExtent(endDate, daysToRun))
  wd <- paste0(getwd(), "/Dependent/")
  m <- readJPEG(paste0(wd,"awhere-logo-plain.jpg"), TRUE)
  rgrob <- rasterGrob(m)
  shp <- readOGR(paste0(wd, "TM_WORLD_BORDERS-0.3"),"TM_WORLD_BORDERS-0.3")
  admin1 <- readOGR(paste0(wd, "Admins"),"globaladmin1")
  #oceans <- readOGR(paste0(wd, "ne_50m_ocean"), "ne_50m_ocean")
  #hillshade <- raster(paste0(wd,"hillshade\\MSR_50M\\MSR_50M.tif"))
  # cn<-odbcDriverConnect("Driver=ODBC Driver 11 for SQL Server; Server=data3dev; Database=CommodityStagingData; trusted_connection=yes")
  
  
  #Loop through each of the regions
  #Update the query that pulls the data from database
  ##Get static datasets from the database
  
  
  #   if (timePeriodIdToRun == 0) {
  #     sqlToExecute <- "SELECT DISTINCT S.LocationSetId, S.PeriodBeginDate, S.PeriodEndDate
  #     FROM [CommodityStagingData].[dbo].[vDynSubComparePOverPET_Sets] S,
  #     (Select LocationSetId, MAX(PeriodEndDate) as PeriodEndDate
  #     FROM [CommodityStagingData].[dbo].[vDynSubComparePOverPET_Sets]
  #     GROUP BY LocationSetId) as MRP
  #     Where MRP.LocationSetId = S.LocationSetId and MRP.PeriodEndDate = S.PeriodEndDate;"
  #   } else {
  #     sqlToExecute <- paste0("SELECT DISTINCT D.LocationSetId, D.PeriodBeginDate, D.PeriodEndDate
  #                            FROM [CommodityStagingData].[dbo].[vDynSubComparePOverPET_Sets] D, 
  #                            (
  #                            SELECT 
  #                            [TimePeriodId]
  #                            ,[BeginDate]
  #                            ,[EndDate]
  #                            FROM 
  #                            [CommodityStagingData].[dbo].[WCRTimePeriods]
  #                            Where 
  #                            TimePeriodId = ", timePeriodIdToRun, ") T
  #                            WHERE D.PeriodBeginDate = T.BeginDate
  #                            and D.PeriodEndDate = T.EndDate;")  
  #   }
  #   
  #   #Get the records from the output table
  #   mostRecentPeriod <- sqlQuery(cn,sqlToExecute,errors=FALSE)
  
  
  
  con <- odbcConnect('Vertica')
  
  sqlToExecute <- "SELECT * FROM libs.DataColumnsToProcess"
  # sqlToExecute <- "SELECT * FROM [CommodityStagingData].[dbo].[WCRDataColumnsToProcess]"
  
  #Get the records from the output table
  dataColumns <- sqlQuery(con,sqlToExecute,errors=FALSE)
  
  dataColumns$MapLegendLabel[is.na(dataColumns$MapLegendLabel)] <- ""
  
  
  if(is.na(locationSetId)) {
    
    sqlToExecute <- "SELECT * from awhere.libs.LocationSetsToRun"
    locationSetId <- sqlQuery(con, sqlToExecute)
    locationSetId <- locationSetId[which(locationSetId$IsActive == 1),]$LocationSetId
    locationSetId <- locationSetId[locationSetId <= 42]
  }
  # Create string of location sets to create charts for
  locationSetToQuery <- locationSetId[1]
  i = 2
  while(i <= length(locationSetId)) {
    locationSetToQuery <- paste0(locationSetToQuery, ",", locationSetId[i])
    i <- i + 1
  }
  
  
  sqlToExecute <- paste0("SELECT LocationSetId
                         ,MapLabelName
                         ,UseSmoothedSurface
                         ,IsSourceLocationSet
                         FROM libs.LocationSetsToRun
                         Where LocationSetId in (", locationSetToQuery, ")")
  print(sqlToExecute)
  
  #Get the records from the output table
  locationSetsToRun <- sqlQuery(con,sqlToExecute,errors=FALSE)
  locationSetsToRun <- locationSetsToRun[order(locationSetsToRun$LocationSetId),]
  print(locationSetsToRun)
  
  sqlToExecute <- "SELECT * FROM libs.ClassBreakTypes"
  
  #Get the records from the output table
  breakTypeData <- sqlQuery(con,sqlToExecute,errors=FALSE)
  
  
  sqlToExecute <- "SELECT *
  FROM libs.CustomBreaks
  order by BreakTypeId, BreakValue"
  
  #Get the records from the output table
  customBreaksData <- sqlQuery(con,sqlToExecute,errors=FALSE)
  
  sqlToExecute <- "SELECT *
  FROM libs.ColorRampCodes
  Order by ColorRampId, RenderOrder"
  
  #Get the records from the output table
  colorRampsData <- sqlQuery(con,sqlToExecute,errors=FALSE)
  
  
  ###############
  ###
  ###############
  #Loop through the location sets that are active
  for (j in 1:nrow(locationSetsToRun)) {
    
    odbcCloseAll()
    con <- odbcConnect('Vertica')
    
    locationSetName <- locationSetsToRun[j,]$MapLabelName
    locationSetId <- locationSetsToRun[j,]$LocationSetId
    mapLabelName <- locationSetsToRun[j,]$MapLabelName
    useRegionSmoothed <- locationSetsToRun[j,]$UseSmoothedSurface
    logoPlacement <- tolower(as.character(locationSetsToRun[j,]$LogoPlacement))
    isSourceLocationSet <- locationSetsToRun[j,]$IsSourceLocationSet
    
    currentRangeExtent <- getRangeExtent(endDate, daysToRun)
    
    periodEndDate <- as.Date(substr(currentRangeExtent[2], 1, 8), "%Y%m%d")
    periodStartDate <- as.Date(substr(currentRangeExtent[1], 1, 8), "%Y%m%d")
    
    yearLabel <- as.numeric(format(as.Date(periodEndDate),"%Y"))
    
    dataPeriod <- periodLabels(periodEndDate,periodStartDate,30)
    dataPeriodAbbr <- gsub("/","",gsub("-","to",gsub(")","",gsub("\\(","",dataPeriod))))  
    
    
    #     
    #     if (useRegionSmoothed == 1)  {
    #       if (isSourceLocationSet == 0) {
    #         tblName <- paste0("[CommodityStagingData].[dbo].[vDynSubComparePOverPET_Sets_Smoothed]")
    #       } else {
    #         tblName <- paste0("[CommodityStagingData].[dbo].[vComparePOverPET_Sets_Smoothed]")
    #       }
    #     } else  {
    #       if (isSourceLocationSet == 0) {
    #         tblName <- paste0("[CommodityStagingData].[dbo].[vDynSubComparePOverPET_Sets]")
    #       } else
    #         tblName <- paste0("[CommodityStagingData].[dbo].[WeatherOutput_ComparePOverPET_Sets]")
    #     }
    
    
    
    ############  SQL TO REPLACE
    ############
    
    #     sqlToExecute <- paste0("SELECT O.LocationId
    #                            ,O.CurrentSumPrec
    #                            ,O.CurrentSumPET
    #                            ,O.CurrentAvgSumPOverSumPET
    #                            ,O.LastYearSumPrec
    #                            ,O.LastYearSumPET
    #                            ,O.LastYearAvgSumPOverSumPET  
    #                            ,O.LTNSumPrec
    #                            ,O.LTNStdSumPrec
    #                            ,O.LTNSumPET
    #                            ,O.LTNStdSumPET
    #                            ,O.LTNAvgSumPOverSumPET
    #                            ,O.LTNStdSumPOverSumPET
    #                            ,O.Z_SumPrec
    #                            ,O.Z_SumPET
    #                            ,O.Z_SumPOverSumPET
    #                            ,O.CurrentSumPrec - O.LastYearSumPrec as DiffSumPrecFromLastYear
    #                            ,O.CurrentSumPrec - O.LTNSumPrec as DiffSumPrecFromLTN
    #                            ,O.CurrentAvgSumPOverSumPET - O.LastYearAvgSumPOverSumPET as DiffAvgSumPOverSumPETFromLastYear
    #                            ,O.CurrentAvgSumPOverSumPET - O.LTNAvgSumPOverSumPET as DiffAvgSumPOverSumPETFromLTN
    #                            ,O.CurrentAvgSumPOverSumPET/(CASE WHEN O.LTNAvgSumPOverSumPET = 0 then 0.1 ELSE O.LTNAvgSumPOverSumPET END) as RatioOfCurrentToLTNPOverPET
    #                            ,((O.CurrentAvgSumPOverSumPET - O.LastYearAvgSumPOverSumPET)/(CASE WHEN O.LastYearAvgSumPOverSumPET = 0 then 0.1 ELSE O.LastYearAvgSumPOverSumPET END) * 100) as DiffPCTP_PETFromLastYear
    #                            ,((O.CurrentAvgSumPOverSumPET - O.LTNAvgSumPOverSumPET)/(CASE WHEN O.LTNAvgSumPOverSumPET = 0 then 0.1 ELSE O.LTNAvgSumPOverSumPET END) * 100) as DiffPCTP_PETFromLTN
    #                            ,((O.CurrentSumPrec - O.LastYearSumPrec)/(CASE WHEN o.LastYearSumPrec = 0 then 0.1 ELSE o.LastYearSumPrec END) * 100) as DiffPCTSumPrecFromLastYear
    #                            ,((O.CurrentSumPrec - O.LTNSumPrec)/(CASE WHEN O.LTNSumPrec = 0 then 0.1 ELSE O.LTNSumPrec END) * 100) as DiffPCTSumPrecFromLTN
    #                            ,G.Latitude, G.Longitude, G.ShapeWKT, G.CentroidWKT 
    #                            From ", tblName ," O 
    #                            INNER JOIN CommodityStagingData.dbo.GlobalGridData G
    #                            ON O.LocationId = G.LocationId
    #                            WHERE O.LocationSetId = ", locationSetId#, subsetSQL 
    #                            ," and O.PeriodBeginDate = '", periodStartDate, "'"
    #                            ," and O.PeriodEndDate = '", periodEndDate, "'"
    #     )
    
    
    #Get the records from the output table
    print(paste0("Connect to smoothing view using SQL for LocationSetId: ",locationSetId))
    
    
    # startYearTemporalId <- 2015010100
    
    currentRangeExtent <- getRangeExtent(endDate, daysToRun)
    
    print(system.time(POverPETDataOut <- getAccumulationsForPeriods(locationSetId, currentRangeExtent[1], currentRangeExtent[2], startYearTemporalId)))
    
    
    
    # print(system.time(POverPETDataOut <- sqlQuery(cn,sqlToExecute,errors=FALSE)))
    
    ext <- extent(c(min(POverPETDataOut$Longitude), max(POverPETDataOut$Longitude), min(POverPETDataOut$Latitude), max(POverPETDataOut$Latitude)))
    
    print ("past the extent")
    
    
    #Remove any outliers
    for (k in 1:nrow(dataColumns)){
      #print (paste0("data column:",k))
      if (is.na(dataColumns[k,]$UseLimitedRange) == FALSE)    {
        if (dataColumns[k,]$UseLimitedRange == 1)    {
          POverPETDataOut[,as.character(dataColumns[k,]$ColumnName)][POverPETDataOut[,as.character(dataColumns[k,]$ColumnName)] > dataColumns[k,]$CapValue] <- dataColumns[k,]$CapValue
          POverPETDataOut[,as.character(dataColumns[k,]$ColumnName)][POverPETDataOut[,as.character(dataColumns[k,]$ColumnName)] < dataColumns[k,]$BaseValue] <- dataColumns[k,]$BaseValue
        }
      }    
    }  
    
    print ("past the limit base and cap")
    
    
    data <- POverPETDataOut
    
    
    locs <- as.character(data$LocationId[1])
    for(i in 2:nrow(data)) {
      locs <- paste0(locs, ",", data$LocationId[i])
    }
    
    fortifiedSpData <- sqlQuery(con, paste0("Select * from libs.FortifiedLocations where id in (", locs, ") order by \"group\", \"order\""))
    fortifiedSpData$group <- as.character(fortifiedSpData$group)
    fortifiedSpData$hole <- FALSE
    fortifiedSpData$order <- c(1:nrow(fortifiedSpData))
    
    data$ShapeWKT <- as.character(data$ShapeWKT)
    data$ShapeWKT <- substr(data$ShapeWKT, 11, nchar(data$ShapeWKT)-2)
    data$ShapeWKT <- gsub(",", "", data$ShapeWKT)
    data$ShapeWKT <- strsplit(data$ShapeWKT, " ")
    data <- unique(data)
    squares <- matrix(ncol = 10, nrow = nrow(data))
    ID <- data$LocationId
    
    w <- which(is.na(data$ShapeWKT))
    
    for(i in 1:nrow(data)) {
      
      squares[i,] <- as.numeric(data$ShapeWKT[[i]])
      
    }
    
    if(length(w) > 0) {
      squares <- squares[-w,]
      ID <- ID[-w]
    }
    # squares <- unique(squares)
    # ID <- unique(ID)
    
    polys <- SpatialPolygons(mapply(function(poly, id) {
      xy <- matrix(poly, ncol=2, byrow=TRUE)
      Polygons(list(Polygon(xy)), ID=id)
    }, split(squares, row(squares)), ID))
    # 
    spData <- SpatialPolygonsDataFrame(polys, data.frame(id=ID, row.names=ID))
    # 
    # print("test")
    # 
    spDataForShpOut <- merge(spData, data, by.x="id", by.y="LocationId")
    # 
    # system.time(fortifiedSpData <- fortify(spData, region="id"))
    
    plot_data <- merge(fortifiedSpData, data, by.x="id", by.y="LocationId")
    
    borders <- shp
    admin1borders <- admin1  
    
    ext <- as(extent(polys), "SpatialPolygons")
    crs(ext) <- crs(borders)
    
    
    print ("crs(ext)")
    
    
    area <- (extent(ext)[2]-extent(ext)[1])*(extent(ext)[4]-extent(ext)[3])
    if(area>250){
      gridspacing <- 5    
    } else if (250>=area & area > 50){
      gridspacing <- 3
    } else if (150>=area & area >50){
      gridspacing <- 2
    } else{
      gridspacing <- 1
    }
    
    ybreaks <- seq(ceiling(extent(ext)[3]), floor(extent(ext)[4]), by=1)[seq(round(extent(ext)[3]), round(extent(ext)[4]), by=1)%%gridspacing==0]
    xbreaks <- seq(ceiling(extent(ext)[1]), floor(extent(ext)[2]), by=1)[seq(round(extent(ext)[1]), round(extent(ext)[2]), by=1)%%gridspacing==0]
    
    # logoplace(ext, logoPlacement)
    
    intersectBorders <- gIntersection(borders, ext, byid=TRUE)
    fortifiedBorders <- fortify(intersectBorders)
    
    intersectAdmin1Borders <- gIntersection(admin1borders, ext, byid=TRUE)
    fortifiedAdmin1Borders <- fortify(intersectAdmin1Borders)  
    
    
    shapefileColumns <- dataColumns[dataColumns$OutputToShapefile == 1,]$ColumnName
    shapefileColumnShortNames <- dataColumns[dataColumns$OutputToShapefile == 1,]$ShortNameForShapefile
    
    #This is where I overwrite the column names with shorter column names that are more appropriate for dbf table paired within shapefile that we wil create
    if (length(shapefileColumns)>0){
      
      #Remove unwanted columns from df that will source shapefile
      spDataForShpOut <- subset(spDataForShpOut, select = c(shapefileColumns)) 
      
      for (k in 1:length(shapefileColumns)){
        names(spDataForShpOut)[names(spDataForShpOut)==as.character(shapefileColumns[k])] <- as.character(shapefileColumnShortNames[k])
      }   
    }
    
    
    
    
    #Set name for shapefile
    shapefileName = paste0(locationSetName, yearLabel, dataPeriodAbbr)
    #Delete any previous shapefiles that have same name
    unlink(paste0(shapefileName, ".*"))
    #Write the shapefile
    writeOGR(spDataForShpOut ,".",shapefileName,driver="ESRI Shapefile")
    #Set name for output csv file
    csvFileName = paste0(locationSetName, yearLabel, dataPeriodAbbr, ".csv")
    #Write the csv file using the dataset pulled from the database for the active locationSet
    write.csv(POverPETDataOut, csvFileName)
    
    ##################
    ###Prep For Map###
    ##################
    
    print ("Before loop through columns")
    
    dataColumns$MapLegendLabel <- as.character(dataColumns$MapLegendLabel)
    dataColumns$MapLegendLabel[which(is.na(dataColumns$MapLegendLabel))] <- ""
    
    for (i in 1:nrow(dataColumns)){
      
      print(i)
      
      if (dataColumns[i,]$OutputToMap==1 | dataColumns[i,]$OutputToChart==1) {
        
        #Select just "LocationId" column and another one. Substitute "layer" with whatever you want to put
        dataToMap <- POverPETDataOut[,c("LocationId", "Latitude", "Longitude", as.character(dataColumns[i,]$ColumnName))]       
        dataToMap <- dataToMap[!is.na(dataToMap[,4]),]
        
        
        brkLabels <- c()
        breakValues <- c()
        brkChecks <- c() 
        
        #     get the breaks for the breakTypeId
        #     are the breaks custom?
        #     are the breaks sequential?
        
        breakTypeId <- dataColumns[i,]$BreakTypeId
        useCustomBreaks <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$UseCustomBreaks
        useColorRamp <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$UseColorRamp
        useSequenceBreaks <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$UseSequenceBreaks
        
        if (useColorRamp == 1) {
          colorRampId <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$ColorRampId
        }
        
        if (useSequenceBreaks == 1) {
          baseSequenceValue <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$BaseSequenceValue
          beginSequenceValue <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$BeginSequenceValue
          incrementSequenceValue <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$IncrementSequenceValue
          endSequenceValue <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$EndSequenceValue
          capSequenceValue <- breakTypeData[breakTypeData$BreakTypeId == breakTypeId,]$CapSequenceValue
          
          breakValues <- c()
          
          if (is.na(baseSequenceValue)==FALSE & baseSequenceValue != beginSequenceValue){
            breakValues <- c(baseSequenceValue)
          }
          
          
          breakValues <-  c(breakValues, seq(from=beginSequenceValue, to=endSequenceValue, by=incrementSequenceValue))
          
          if (is.na(capSequenceValue)==FALSE  & endSequenceValue != capSequenceValue){
            breakValues <- c(breakValues, capSequenceValue)
          }
          
          colorValues <- colorRampsData[colorRampsData$ColorRampId == colorRampId,]$ColorCode
          
          colorValues <- colorRampPalette(colorValues)(length(breakValues)-1) 
          
        }
        
        if (useCustomBreaks == 1) {
          breakValues <- customBreaksData[customBreaksData$BreakTypeId == breakTypeId,]$BreakValue
          colorValues <- unique(customBreaksData[customBreaksData$BreakTypeId == breakTypeId,]$BreakColor)
        }
        
        brkLabels <- breakLabels(breakValues)
        brkChecks <- breakChecks(breakValues)
        
        w <- which(plot_data[,as.character(dataColumns[i,]$ColumnName)] > breakValues[length(breakValues)])
        plot_data[w,as.character(dataColumns[i,]$ColumnName)] <- breakValues[length(breakValues)] - 1
        
        w <- which(plot_data[,as.character(dataColumns[i,]$ColumnName)] < breakValues[1])
        plot_data[w,as.character(dataColumns[i,]$ColumnName)] <- breakValues[1] + 1
        
        obsmeanlev=cut(as.numeric(plot_data[,as.character(dataColumns[i,]$ColumnName)]),breaks=breakValues, include.lowest=TRUE, dig.lab=10) #, labels=brkLabels)
        
        
        #       given the breakTypeName get the breakTypeId from the database
        #       get the breaks for the breakTypeId
        #         are the breaks custom?
        #         are the breaks sequential?
        #       get the labels for the breaks
        #       get the checks for the breaks
        #       get the colors for the breakTypeId
        #       does this breakTypeId use custom breaks?
        #         if so, then grab the colors from WCRCustomBreaks
        #         if not, then see if it uses a standard color ramp and get that ColorRampId
        #           if so, then grab the colors from WCRColorRampCodes where ColorRampId = specified ColorRampId
        
        
        #######################
        ###Make the map plot###
        #######################
        
        
        #######################
        ###Save the map plot###
        #######################
        
        
        #         
        #         commoditySetToQuery <- commodityId[1]
        #         i = 2
        #         while(i <= length(commodityId)) {
        #           commoditySetToQuery <- paste0(commoditySetToQuery, ",", commodityId[i])
        #           i <- i + 1
        #         }
        
        
        #loop through commodities that are related to active locationSetId
        sqlToExecute <- paste0("SELECT CommodityId, CommodityName, 1 as UseSubset 
                               FROM libs.Commodities
                               Where CommodityId in (Select CommodityId From libs.CommoditySpecificSubsets Where LocationSetId = ",locationSetId,
                               ") UNION ALL Select 0 as CommodityId, NULL as CommodityName, 0 as UseSubset")
        print(commodityIdToUse)
        if(commodityIdToUse == 0 & !is.na(commodityIdToUse)) {
          commoditiesToRun <- sqlQuery(con, sqlToExecute, errors = T)
          commoditiesToRun <- commoditiesToRun %>% filter(CommodityId == 0)
        } else {
          commoditiesToRun <- sqlQuery(con,sqlToExecute,errors=TRUE)
          if(!is.na(commodityIdToUse)) {
            commoditiesToRun <- commoditiesToRun[commoditiesToRun$CommodityId %in% commodityIdToUse,]
          }
        }
        
        rawChartData <- as.data.frame(data[,c(as.character(dataColumns[i,]$ColumnName), "LocationId")])
        
        for (l in 1:nrow(commoditiesToRun)){
          
          commodityId <- commoditiesToRun[l,]$CommodityId
          commodityName <- commoditiesToRun[l,]$CommodityName
          useSubset <- commoditiesToRun[l,]$UseSubset
          
          if (is.na(commodityName) == TRUE) {
            useMapLabelName <- paste0(mapLabelName, " ", yearLabel)
          } else {
            useMapLabelName <- paste0(commodityName,"-",mapLabelName, " ", yearLabel)   
          }
          
          #########################
          ###Make the chart plot###
          #########################
          
          if (dataColumns[i,]$OutputToChart==1){
            
            if (useSubset == 0) {
              useChartData <- rawChartData
            } else {
              #subset ChartData to only be the grid cells that have been associated with the active commodityId
              sqlToExecute <- paste0("SELECT Distinct LocationId 
                                     FROM libs.CommodityAreas
                                     WHERE CommodityId = ", commodityId,
                                     " and LocationId in (Select LocationId From libs.LocationSetMembers Where LocationSetId = ", locationSetId ,")" )
              
              chartLocations <- sqlQuery(con,sqlToExecute,errors=TRUE)          
              
              useChartData <- as.data.frame(rawChartData[rawChartData$LocationId %in% chartLocations$LocationId, ]) 
            }
            
            w <- which(useChartData[,1] > breakValues[length(breakValues)])
            useChartData[w,1] <- breakValues[length(breakValues)] - 1
            
            w <- which(useChartData[,1] < breakValues[1])
            useChartData[w,1] <- breakValues[1] + 1
            
            
            #plot regionMap with commodity overlay
            useChartData$breaks <- cut(as.numeric(useChartData[,as.character(dataColumns[i,]$ColumnName)]),breaks=breakValues, include.lowest=T, labels=brkLabels)
            useChartData$out <- useChartData[,as.character(dataColumns[i,]$ColumnName)]
            useChartData <- useChartData[!is.na(useChartData$breaks),]
            useChartData <- as.data.table(useChartData)[,.(count=.N), by=.(breaks)]
            useChartData <- as.data.frame(useChartData)
            
            if (nrow(data.frame(brkLabels[!(brkLabels %in% useChartData$breaks)]))>0) 
            {
              df <- data.frame(breaks=brkLabels[!(brkLabels %in% useChartData$breaks)],count=0)
              useChartData <- rbind(useChartData,df)
            }
            
            #Plot using subsetting ChartData
            #Chart rendering
            
            chart <- ggplot(data=useChartData, aes(x=breaks, y=count, fill=factor(breaks))) +
              ggtitle(paste0(useMapLabelName,"\n",dataColumns[i,]$MapTitle," ", dataPeriod)) +  
              geom_bar(stat="identity", width = 0.8) +
              geom_text(data=useChartData, aes(x=breaks,y=count+(max(count)*0.03),label=paste0(round(count/sum(count),2)*100, "%")), size = 4, family="Geogrotesque SmBd", color="#4d4d4d") + 
              scale_fill_manual(values = as.character(unique(colorValues)), name = "") + theme(legend.position = "none") +
              theme(plot.title = element_text(size=20, family="Geogrotesque SmBd")) +
              theme(axis.title = element_text(size=18, family="Geogrotesque SmBd", hjust=0.9, color="#4d4d4d")) + 
              theme(axis.text = element_text(size=12, family="Geogrotesque SmBd", color="#333333")) +
              theme(panel.background = element_blank()) +
              theme(panel.grid.major.y =  element_line(colour = "#cccccc")) + 
              theme(panel.grid.minor.y =  element_blank()) + 
              ylab(paste0(dataColumns[i,]$ChartYLabel,"\n")) + xlab(paste0("\n",dataColumns[i,]$ChartXLabel))
            
            if (nchar(paste0(useChartData$breaks, collapse=""))> 75){
              chart <- chart + theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
            }  
            
            ggsave(filename = gsub(" ","", paste0(useMapLabelName, dataColumns[i,]$ColumnName, dataPeriodAbbr, "_Chart.png")))
            
          }
          
          ###########################
          
          if (dataColumns[i,]$OutputToMap==1){
            
            regionToMap <- ggplot()
            
            regionToMap <- regionToMap + geom_polygon(data = filter(fortifiedBorders), 
                                                      aes(x = long, y = lat, group = group),
                                                      color = "#737373", fill = "#c7b59b", size = .8) + coord_equal(ratio=1)
            
            plot_data$obsmeanlev <- obsmeanlev
            
            regionToMap <- regionToMap + geom_polygon(data = plot_data, aes(x=long, y=lat, group = group, fill = obsmeanlev)) + 
              geom_polygon() + theme(text=element_text(size=16, family="Geogrotesque SmBd")) + 
              ggtitle(paste0(useMapLabelName,"\n", dataColumns[i,]$MapTitle," ", dataPeriod,"\n")) + 
              ylab("") + xlab("") +
              theme(panel.border = element_rect(color="#0d0d0d", fill="transparent"))  
            
            
            regionToMap <- regionToMap +
              scale_fill_manual(name=dataColumns[i,]$MapLegendLabel,values=as.character(unique(colorValues[brkChecks %in% as.character(unique(obsmeanlev))])),
                                breaks=levels(obsmeanlev),labels=brkLabels, guide = guide_legend(ncol = 1))
            #                 geom_polygon(data = filter(fortifiedAdmin1Borders), 
            #                              aes(x = long, y = lat, group = group),
            #                              color = "black", fill = NA, size = .3)           
            
            regionToMap <- regionToMap + theme(panel.background = element_rect(fill = "#bfe4f2", colour="black"))
            
            regionToMap <- regionToMap + 
              theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
              theme(legend.title = element_text(color="#333333", size=12, family="Geogrotesque SmBd")) + 
              theme(legend.text = element_text(color="#333333", size=10, family="Geogrotesque Rg" )) +
              theme(axis.text.x = element_text(color="#333333", size=12, angle=30, vjust=1)) +
              theme(axis.text.y = element_text(color="#333333", size=12, angle=30, vjust=1))
            
            
            regionToMap <- regionToMap + geom_polygon(data = filter(fortifiedAdmin1Borders), 
                                                      aes(x = long, y = lat, group = group),
                                                      color = "#b3b3b3", fill = NA, size = .3)  # #d6d6d6       
            
            regionToMap <- regionToMap + geom_polygon(data = filter(fortifiedBorders), 
                                                      aes(x = long, y = lat, group = group),
                                                      color = "#737373", fill = NA, size = .8) 
            
            regionToMap <- regionToMap + 
              scale_y_continuous(expand = c(0,0), breaks=ybreaks, labels=paste0(ybreaks,"°")) + 
              scale_x_continuous(expand = c(0,0), breaks=xbreaks, labels=paste0(xbreaks,"°")) +
              geom_vline(xintercept=xbreaks, col="#ffffff", linetype=2, alpha=.5) +
              geom_hline(yintercept=ybreaks, col="#ffffff", linetype=2, alpha=.5)
            
            # regionToMap <- regionToMap + guide_legend(ncol = 1)
            
            
            if (commodityId != 0) {
              
              print (paste0("Inside Commodity ", locationSetId, "-", commodityId))
              
              sqlToExecute <- paste0("SELECT OverlayShapesWKT, UseProduction80
                                     FROM libs.CommoditySpecificSubsets
                                     WHERE LocationSetId = ", locationSetId, " and CommodityId = ", commodityId)
              
              commodityOverlays <- sqlQuery(con,sqlToExecute,errors=TRUE)  
              
              
              commodityOverlay <- readWKT(as.character(commodityOverlays$OverlayShapesWKT), id = NULL, p4s = CRS("+init=epsg:4326"))
              
              extForOverlay <- spTransform(ext, CRS("+init=epsg:4326"))
              
              if(class(commodityOverlay) == "SpatialCollections") {
                IntersectedCommodityOverlay <- gIntersection(commodityOverlay@polyobj, extForOverlay, byid=TRUE)
              } else {
                IntersectedCommodityOverlay <- gIntersection(commodityOverlay, extForOverlay, byid=TRUE)
              }
              
              
              splitOverlay <- disaggregate(IntersectedCommodityOverlay)
              
              splitOverlay.df <- data.frame( ID=1:length(splitOverlay)) 
              
              #splitOverlay.df$area <- gArea(IntersectedCommodityOverlay)
              
              overlaySpDf <- SpatialPolygonsDataFrame(splitOverlay, splitOverlay.df) 
              
              overlaySpDf@data$regionId <- toupper(letters[1:length(splitOverlay)])
              
              overlayCenters <- SpatialPointsDataFrame(gCentroid(overlaySpDf, byid=TRUE), 
                                                       overlaySpDf@data, match.ID=FALSE)
              
              dataForLabels <- as.data.frame( cbind(regionId = overlayCenters@data[,"regionId"],Longitude = overlayCenters@coords[,"x"],Latitude = overlayCenters@coords[,"y"]))
              
              
              commodityPlot <- regionToMap + geom_polygon(data = IntersectedCommodityOverlay, 
                                                          aes(x = long, y = lat, group = group),
                                                          color = "#1a1a1a", fill = NA, size = 1.5,
                                                          linetype = "twodash") +            
                geom_text(data = dataForLabels, aes(label = as.character(regionId), x = as.numeric(as.character(Longitude)), y = as.numeric(as.character(Latitude))))  
              
            }  
            
            #commodityPlot
            
            ggsave(filename = gsub(" ","",paste0(useMapLabelName, dataColumns[i,]$ColumnName, dataPeriodAbbr, "_Map.png")))
            
          }
          
          ######
          
          
          
          
          
          ##---------------------this is the break
        } #This is for loop through commodities
        
      }  #This is for exceptions where there is no chart or map need
    } # This is for the data column loop
    print(gsub(" ", "", paste0(mapLabelName, yearLabel)))
    print(dataPeriodAbbr)
    zipem(gsub(" ", "", paste0(mapLabelName, yearLabel)), dataPeriodAbbr, "png") 
  }  #This is for locationSets loop
  
  
  #Close the connection
  odbcCloseAll()
}