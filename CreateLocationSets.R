library(RODBC)
library(rgeos)
library(wkb)


doesLocationSetExist <- function(locationSetName){
  
  con <- odbcConnect('Vertica')
  
  dfLocationSetId <- sqlQuery(con, paste0("SELECT LocationSetId from libs.LocationSets Where LocationSetName = '",locationSetName,"'"))
  
  if (nrow(dfLocationSetId) > 0) {
    locationSetId <- as.integer(dfLocationSetId[1])
  }
  else {locationSetId <- 0}
  
  return(locationSetId)
  
}


createLocationSetFromShapeWKT <- function(locationSetName, shapeWKT, subsetOfLocationSetId){
  
  con <- odbcConnect('Vertica')
  
  
  locationSetId <- doesLocationSetExist(locationSetName)
  
  g <- readWKT(shapeWKT)
  
  if (gIsValid(g)==TRUE){
    
    if (locationSetId == 0) {
      
      sqlQuery(con, paste0("INSERT INTO libs.LocationSets (LocationSetName, ShapeWKT) Values ('",locationSetName,"','",shapeWKT,"');"))
      
      locationSetId <- doesLocationSetExist(locationSetName)
      
    }
    
    sqlToExecute <- paste0("INSERT INTO libs.LocationSetMembers (LocationSetId, LocationId) ",
                           "SELECT ", locationSetId, " as LocationSetId, LocationId From awhere.libs.GlobalGridXGridY G ",
                           "Where ST_Intersects(CentroidGeom,ST_GeomFromText('",shapeWKT,"',4326));")
    
    
    sqlToExecute <- gsub("[\r\n]", "", sqlToExecute)
    
    
    sqlQuery(con, sqlToExecute)
    
    
  }
  
  
  return(locationSetId)
  
}


createLocationSetFromShapeWKB <- function(locationSetName, shapeWKB){
  
  con <- odbcConnect('Vertica')
  
  locationSetId <- doesLocationSetExist(locationSetName)
  
  g <- readWKB(shapeWKB, id = NULL, proj4string = CRS("+init=epsg:4326"))
  
  if (gIsValid(g)==TRUE){
    
    if (locationSetId == 0) {
      
      sqlQuery(con, paste0("INSERT INTO libs.LocationSets (LocationSetName) Values ('",locationSetName,"');"))
      
      locationSetId <- doesLocationSetExist(locationSetName)
      
    }
    
    sqlToExecute <- paste0("INSERT INTO libs.LocationSetMembers (LocationSetId, LocationId) ",
                           "SELECT ", locationSetId, " as LocationSetId, LocationId From awhere.libs.GlobalGridXGridY G ",
                           "Where ST_Intersects(CentroidGeom,ST_GeomFromWKB('",shapeWKB,"',4326));")
    
    
    sqlToExecute <- gsub("[\r\n]", "", sqlToExecute)
    
    
    sqlQuery(con, sqlToExecute)
    
    
  }
  
  
  return(locationSetId)
  
}




createLocationSetFromLocationName <- function(locationSetName, requestedLocations){
  
  con <- odbcConnect('Vertica')
  
  locationSetId <- doesLocationSetExist(locationSetName)
  
  
  if (locationSetId == 0) {
    
    sqlQuery(con, paste0("INSERT INTO libs.LocationSets (LocationSetName) Values ('",locationSetName,"');"))
    
    locationSetId <- doesLocationSetExist(locationSetName)
    
  }    
  
  
  sqlToExecute <- paste0("INSERT INTO libs.LocationSetMembers (LocationSetId, LocationId) ",
                         "SELECT ", locationSetId, " as LocationSetId, G.LocationId
                          From awhere.libs.GlobalGridXGridY G, dev.AdminLocations A
                          Where ST_Intersects(G.CentroidGeom,A.LocationGeom)
                          and A.LocationName IN (",paste("'",as.character(requestedLocations),"'",collapse=", ",sep=""),");")
  
  
  
  #   'select LocationId
  #   From awhere.libs.GlobalGridXGridY G, dev.AdminLocations A
  #   Where ST_Intersects(G.CentroidGeom,ST_GeomFromText(A.LocationWKT,4326))
  #   and A.LocationName IN (',paste("'",as.character(locationName),"'",collapse=", ",sep=""),');'
  
  sqlToExecute <- gsub("[\r\n]", "", sqlToExecute)
  
  
  sqlQuery(con, sqlToExecute)
  
  
  return(locationSetId)  
  
  #return (sqlToExecute)
  
}

